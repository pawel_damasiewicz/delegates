# Psr-15 sandbox

## Set up

* Configure server
    * documment root `public/`

    * documment index: `public/index.php`

* Instal dependencies
    * `composer install`

* Unit tests
    * `vendor/bin/phpunit`
    
## Docker

* Build and run whole app - `docker-compose up`

* Test code - TBA

## Resources

* [Concept By Ocramius](https://ocramius.github.io/from-helpers-to-middleware)
* [Pimple](http://pimple.sensiolabs.org/)
* [Dispatcher Idea](https://github.com/mindplay-dk/middleman)
* [Diactoros](https://zendframework.github.io/zend-diactoros/)
* [Stratigility](https://docs.zendframework.com/zend-stratigility/)
* [psr15-middlewares](https://github.com/middlewares/psr15-middlewares)
