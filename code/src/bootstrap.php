<?php

use Pdam\Provider\App;
use Pdam\Provider\Config;
use Pdam\Provider\ErrorHandler;
use Pdam\Provider\Middleware;
use Pdam\Provider\Routes;
use Pdam\Provider\Template;
use Pimple\Container;

/**
 * Load files
 */
require(__DIR__ . '/../vendor/autoload.php');

/**
 * Create container
 */
$container = new Container();

/**
 * Register providers - order matters
 */
$container->register(new Config(require(__DIR__ . '/../config.php')));
$container->register(new ErrorHandler());
$container->register(new Template());
$container->register(new Middleware());
$container->register(new Routes());
$container->register(new App());

return $container['app'];