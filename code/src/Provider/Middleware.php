<?php namespace Pdam\Provider;

use League\Plates\Engine;
use Middlewares\ContentEncoding;
use Middlewares\ContentLanguage;
use Middlewares\ContentType;
use Pdam\Middleware\Admin\Post\Get;
use Pdam\Middleware\Api\Post;
use Pdam\Middleware\Api\Posts;
use Pdam\Middleware\Api\PostsCollection;
use Pdam\Middleware\CallablePipe;
use Pdam\Middleware\Home;
use Pdam\Middleware\Render;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Zend\Diactoros\Response;
use Zend\Stratigility\Middleware\NotFoundHandler;
use Zend\Stratigility\MiddlewarePipe;
use Zend\Stratigility\MiddlewarePipeInterface;

class Middleware implements ServiceProviderInterface
{
    public function register(Container $container)
    {
        $container[MiddlewarePipe::class] = $container->factory(function () {
            return new MiddlewarePipe();
        });

        $container[MiddlewarePipeInterface::class] = $container->factory(function ($container) {
            return new CallablePipe($container[MiddlewarePipe::class]);
        });

        $container[NotFoundHandler::class] = function (Container $container) {
            return new NotFoundHandler($container['factory.response']);
        };

        $container['factory.response'] = function (Container $container) {
            return $container['factory.response.empty'];
        };

        $container['factory.response.empty'] = $container->factory(function () {
            return function () {
                return new Response();
            };
        });

        $container[ContentType::class] = function ($container) {
            return new ContentType($container['config']['content']['types']);
        };

        $container[ContentLanguage::class] = function ($container) {
            return new ContentLanguage($container['config']['content']['languages']);
        };

        $container[ContentEncoding::class] = function ($container) {
            return new ContentEncoding($container['config']['content']['encodings']);
        };

        $container[Render::class] = function (Container $container) {
            // Warning! getContainer() returns internal container for Plates Engine
            return new Render(
                $container[Engine::class]
                    ->getContainer()
                    ->get('renderTemplate'),
                $container[NotFoundHandler::class]
            );
        };

        $container[Home::class] = function () {
            return new Home();
        };

        $container[PostsCollection::class] = function () {
            return new PostsCollection();
        };

        $container[Post::class] = function () {
            return new Post();
        };

        $container[Posts::class] = function ($container) {
            return new Posts($container[PostsCollection::class], $container[Post::class]);
        };

        $container[\Pdam\Middleware\Admin\Post::class] = function (Container $container) {
            return new \Pdam\Middleware\Admin\Post($container[Get::class]);
        };

        $container[Get::class] = function (Container $container) {
            return new Get($container[Get\Collection::class], $container[Get\Item::class]);
        };

        $container[Get\Item::class] = function () {
            return new Get\Item();
        };

        $container[Get\Collection::class] = function (Container $container) {
            return new Get\Collection($container['template.admin.post.get.collection']);
        };
    }
}
