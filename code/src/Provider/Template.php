<?php

namespace Pdam\Provider;


use League\Plates\Engine;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class Template implements ServiceProviderInterface
{
    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $container A container instance
     */
    public function register(Container $container)
    {
        $container[Engine::class] = function (Container $container) {
            return Engine::create($container['config']['template']['rootPath']);
        };

        $container['template.admin.post.get.collection'] = $container->factory(function (Container $container) {
            return new \League\Plates\Template(
                $container['config']['template']['admin']['post']['get']['collection']
            );
        });
    }
}