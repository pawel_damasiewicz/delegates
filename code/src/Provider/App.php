<?php namespace Pdam\Provider;

use Middlewares\ContentEncoding;
use Middlewares\ContentLanguage;
use Middlewares\ContentType;
use Pdam\Middleware\ErrorHandler;
use Pdam\Middleware\Render;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Zend\Stratigility\MiddlewarePipeInterface;

class App implements ServiceProviderInterface
{
    /**
     * @param Container $container
     */
    public function register(Container $container)
    {

        $container['app'] = function (Container $container) {
            $app = $container[MiddlewarePipeInterface::class];

            $app->pipe($container[ErrorHandler::class]);

            $app->pipe($container[ContentLanguage::class]);
            $app->pipe($container[ContentEncoding::class]);
            $app->pipe($container[ContentType::class]);

            $app->pipe($container['routes']);

            // Render or 404
            $app->pipe($container[Render::class]);

            return $app;
        };
    }
}
