<?php namespace Pdam\Provider;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class Config implements ServiceProviderInterface
{
    /**
     * @var array
     */
    private $config;

    /**
     * Config constructor.
     * @param array $config
     */
    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * @param Container $container
     */
    public function register(Container $container)
    {
        $container['config'] = $this->config;
    }
}
