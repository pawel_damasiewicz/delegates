<?php namespace Pdam\Provider;

use Pdam\Middleware\Admin\Post;
use Pdam\Middleware\Api\Posts;
use Pdam\Middleware\Home;
use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Zend\Stratigility\MiddlewarePipeInterface;
use function Zend\Stratigility\path;

class Routes implements ServiceProviderInterface
{
    public function register(Container $container)
    {
        $container['routes'] = function (Container $container) {
            /** @var MiddlewarePipeInterface $routes */
            $routes = $container[MiddlewarePipeInterface::class];

            $routes->pipe(path('/home', $container[Home::class]));
            $routes->pipe(path('/api', $container['routes.api']));
            $routes->pipe(path('/admin', $container['routes.admin']));

            return $routes;
        };

        $container['routes.api'] = function (Container $container) {
            // Api
            /** @var MiddlewarePipeInterface $api */
            $api = $container[MiddlewarePipeInterface::class];
            $api->pipe(path('/posts', $container[Posts::class]));

            return $api;
        };

        $container['routes.admin'] = function (Container $container) {
            $admin = $container[MiddlewarePipeInterface::class];
            $admin->pipe(path('/posts', $container[Post::class]));

            return $admin;
        };
    }
}
