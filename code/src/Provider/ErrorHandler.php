<?php

namespace Pdam\Provider;


use League\BooBoo\BooBoo;
use League\BooBoo\Formatter\HtmlFormatter;
use League\BooBoo\Formatter\NullFormatter;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ErrorHandler implements ServiceProviderInterface
{
    /**
     * Registers services on the given container.
     *
     * This method should only be used to configure services and parameters.
     * It should not get services.
     *
     * @param Container $container A container instance
     */
    public function register(Container $container)
    {
        $container[\Pdam\Middleware\ErrorHandler::class] = function (Container $container) {
            return new \Pdam\Middleware\ErrorHandler($container[BooBoo::class]);
        };

        $container[BooBoo::class] = function (Container $container) {
            return new BooBoo($container['booBoo.formatters']);
        };

        $container['booBoo.formatters'] = function (Container $container) {
            return [
                $container[NullFormatter::class],
                $container[HtmlFormatter::class]
            ];
        };

        $container[NullFormatter::class] = function (Container $container) {
            $null = new NullFormatter();
            $null->setErrorLimit(E_ALL);

            return $null;
        };

        $container[HtmlFormatter::class] = function (Container $container) {
            $html = new HtmlFormatter();
            $html->setErrorLimit(E_ERROR | E_WARNING | E_USER_ERROR | E_USER_WARNING);

            return $html;
        };
    }
}