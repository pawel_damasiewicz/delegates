<?php

namespace Pdam\Model;


interface ResourceCollectionInterface extends \SeekableIterator, \ArrayAccess, \Serializable, \Countable
{
}