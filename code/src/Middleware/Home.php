<?php

namespace Pdam\Middleware;

use League\Plates\Template;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class Home implements MiddlewareInterface
{
    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface
    {
        return $handler->handle($request->withAttribute(
            'template', (new Template('front/home'))->withData([
            'name' => $this->getName($request)
        ])
        ));
    }

    /**
     * @param ServerRequestInterface $request
     * @return string
     */
    private function getName(ServerRequestInterface $request)
    {
        return trim($request->getUri()->getPath(), '/');
    }
}
