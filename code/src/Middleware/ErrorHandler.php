<?php

namespace Pdam\Middleware;


use League\BooBoo\BooBoo;
use League\BooBoo\Exception\NoFormattersRegisteredException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response;

class ErrorHandler implements MiddlewareInterface
{
    /**
     * @var BooBoo
     */
    private $booBoo;

    /**
     * ErrorHandler constructor.
     * @param BooBoo $booBoo
     */
    public function __construct(BooBoo $booBoo)
    {
        $this->booBoo = $booBoo;
    }

    /**
     * Process an incoming server request and return a response, optionally delegating
     * response creation to a handler.
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        try {
            $this->booBoo->register();
        } catch (NoFormattersRegisteredException $e) {
            $res = new Response();
            $res->getBody()->write('Your Error Handler configuration is fu**** up. Check your Service Providers.');

            return $res;
        }

        return $handler->handle($request);
    }
}