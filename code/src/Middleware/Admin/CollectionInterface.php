<?php

namespace Pdam\Middleware\Admin;


use Pdam\Model\ResourceCollectionInterface;
use Psr\Http\Server\MiddlewareInterface;

interface CollectionInterface extends MiddlewareInterface
{
    public function getCollection(): ResourceCollectionInterface;
}