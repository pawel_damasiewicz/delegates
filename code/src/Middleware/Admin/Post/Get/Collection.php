<?php

namespace Pdam\Middleware\Admin\Post\Get;


use League\Plates\Template;
use Pdam\Middleware\Admin\CollectionInterface;
use Pdam\Model\ResourceCollectionInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class Collection implements MiddlewareInterface, CollectionInterface
{
    /**
     * @var Template
     */
    private $template;

    /**
     * Collection constructor.
     * @param Template $template
     */
    public function __construct(Template $template)
    {
        $this->template = $template;
    }

    /**
     * Process an incoming server request and return a response, optionally delegating
     * response creation to a handler.
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        return $handler->handle($request->withAttribute(
            'template', $this->template->withData(['posts' => $this->getCollection()])
        ));
    }

    /**
     * @return ResourceCollectionInterface
     */
    public function getCollection(): ResourceCollectionInterface
    {
        $items = [
            [
                'title' => 'About me',
                'author' => 'pawel.damasiewicz',
                'body' => 'lorem ipsum dolor est...',
            ],
            [
                'title' => 'Test',
                'author' => 'pawel.damasiewicz',
                'body' => 'lorem ipsum dolor est...',
            ],
            [
                'title' => 'Hello World!',
                'author' => 'pawel.damasiewicz',
                'body' => 'lorem ipsum dolor est...',
            ],
        ];

        foreach ($items as $i => &$item): $item['index'] = $i; endforeach;

        return new \Pdam\Model\Post\Collection($items);
    }
}