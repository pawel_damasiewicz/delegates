<?php

namespace Pdam\Middleware\Admin\Post;


use Pdam\Middleware\Admin\Post\Get\Collection;
use Pdam\Middleware\Admin\Post\Get\Item;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class Get
 * @package Pdam\Middleware\Admin\Post
 */
class Get implements MiddlewareInterface
{
    /**
     * @var Collection
     */
    private $collection;

    /**
     * @var Item
     */
    private $item;

    /**
     * Get constructor.
     * @param Collection $collection
     * @param Item $item
     */
    public function __construct(Collection $collection, Item $item)
    {
        $this->collection = $collection;
        $this->item = $item;
    }

    /**
     * Process an incoming server request and return a response, optionally delegating
     * response creation to a handler.
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        switch ($this->getType($request)) {
            case 'collection':
                return $this->collection->process($request, $handler);
            case 'item':
                return $this->item->process($request, $handler);
        }
    }

    /**
     * @param ServerRequestInterface $request
     * @return string
     */
    private function getType(ServerRequestInterface $request)
    {
        $path = $request->getUri()->getPath();

        return (trim($path, "/") === '') ? 'collection' : 'item';
    }
}