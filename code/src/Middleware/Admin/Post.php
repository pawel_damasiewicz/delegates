<?php

namespace Pdam\Middleware\Admin;


use Pdam\Middleware\Admin\Post\Get;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response;

class Post implements MiddlewareInterface
{
    /**
     * @var Get
     */
    private $get;

    /**
     * Post constructor.
     * @param Get $get
     */
    public function __construct(Get $get)
    {
        $this->get = $get;
    }

    /**
     * TODO: CRUD - posts:create, posts:update, posts:delete, posts:new, posts:index, posts:show, posts:edit
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        switch ($request->getMethod()) {
            case 'GET':
                return $this->processGet($request, $handler);
            case 'POST':
                return $this->processPost($request, $handler);
            case 'PATCH':
                return $this->processPatch($request, $handler);
            case 'DELETE':
                return $this->processDelete($request, $handler);
            default:
                return $this->methodNotAllowed($request);
        }
    }

    /**
     * @param $request
     * @param $handler
     * @return ResponseInterface
     */
    private function processGet($request, $handler)
    {
        return $this->get->process($request, $handler);
    }

    /**
     * @param $request
     * @param $handler
     */
    private function processPost($request, $handler)
    {
        // TODO: implement post handler.
    }

    /**
     * @param $request
     * @param $handler
     */
    private function processPatch($request, $handler)
    {
        // TODO: implement patch handler.
    }

    /**
     * @param $request
     * @param $handler
     */
    private function processDelete($request, $handler)
    {
        // TODO: implement delete handler.
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    private function methodNotAllowed(ServerRequestInterface $request)
    {
        $response = new Response();
        $response->getBody()->write(
            sprintf('Method not allowed: [%s]', $request->getMethod())
        );

        return $response->withStatus(405);
    }
}