<?php

namespace Pdam\Middleware;


use League\Plates\RenderTemplate;
use League\Plates\Template;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response;
use Zend\Stratigility\Middleware\NotFoundHandler;

class Render implements MiddlewareInterface
{
    /**
     * @var RenderTemplate
     */
    private $renderer;

    /**
     * @var Template
     */
    private $fallbackTemplate;

    /**
     * @var NotFoundHandler
     */
    private $notFoundHandler;

    /**
     * Render constructor.
     * @param RenderTemplate $renderer
     * @param NotFoundHandler $notFoundHandler
     */
    public function __construct(RenderTemplate $renderer, NotFoundHandler $notFoundHandler)
    {
        $this->renderer = $renderer;
        $this->fallbackTemplate = new Template('error/template/fallback');
        $this->notFoundHandler = $notFoundHandler;
    }

    /**
     * Process an incoming server request and return a response, optionally delegating
     * response creation to a handler.
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if ($request->getAttribute('template', false)) {
            $response = new Response();
            $response->getBody()->write(
                $this->renderer->renderTemplate(
                    $request->getAttribute('template')
                )
            );

            return $response;
        }

        return $this->notFoundHandler->process($request, $handler);
    }
}