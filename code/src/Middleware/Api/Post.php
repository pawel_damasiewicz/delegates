<?php

namespace Pdam\Middleware\Api;


use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response;

class Post implements MiddlewareInterface
{
    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface
    {
        $post = [
            'title' => 'post 1',
            'author' => 'pawel.damasiewicz',
            'body' => 'lorem ipsum dolor est...',
        ];

        $response = new Response();
        $response->getBody()->write(json_encode($post));
        $response = $response->withHeader('content-type', 'application/json');

        return $response;
    }
}
