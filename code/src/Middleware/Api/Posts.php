<?php

namespace Pdam\Middleware\Api;


use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Zend\Diactoros\Response;

class Posts implements MiddlewareInterface
{
    /**
     * @var PostsCollection
     */
    private $index;

    /**
     * @var Post
     */
    private $show;

    /**
     * Posts constructor.
     * @param PostsCollection $index
     * @param Post $show
     */
    public function __construct(
        PostsCollection $index,
        Post $show
    )
    {
        $this->index = $index;
        $this->show = $show;
    }

    /**
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     */
    public function process(
        ServerRequestInterface $request,
        RequestHandlerInterface $handler
    ): ResponseInterface
    {
        if ($this->isIndex($request)) {
            return $this->index->process($request, $handler);
        }

        if ($this->isShow($request)) {
            return $this->show->process($request, $handler);
        }

        $response = new Response();
        $response->getBody()->write('posts');

        return $response;
    }

    /**
     * @param ServerRequestInterface $request
     * @return bool
     */
    private function isIndex(ServerRequestInterface $request)
    {
        $isGet = $request->getMethod() === 'GET';
        $isEndpoint = trim($request->getUri()->getPath(), '/') === '';

        return $isGet && $isEndpoint;
    }

    /**
     * @param ServerRequestInterface $request
     * @return bool
     */
    private function isShow(ServerRequestInterface $request)
    {
        $isGet = $request->getMethod() === 'GET';
        $hasId = trim($request->getUri()->getPath(), '/') !== '';

        return $isGet && $hasId;
    }
}
