<?php

return [
    'env' => [
        'name' => getenv('APP_ENV') ? getenv('APP_ENV') : 'development',
    ],
    'content' => [
        'types' => [
            'html' => ['mime-type' => ['text/html']],
        ],
        'languages' => ['en', 'gl', 'es'],
        'encodings' => ['gzip', 'deflate'],
    ],
    'template' => [
        'rootPath' => __DIR__ . '/templates/',
        'admin' => [
            'post' => [
                'get' => [
                    'collection' => 'admin/post/get/collection'
                ],
            ],
        ],
    ],
];
