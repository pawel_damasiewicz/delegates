<?php

use Zend\Diactoros\Server;

$app = require __DIR__ . '/../src/bootstrap.php';

(Server::createServer(
    $app,
    $_SERVER,
    $_GET,
    $_POST,
    $_COOKIE,
    $_FILES
))->listen();
